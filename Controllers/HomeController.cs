﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Opdracht4Chat.Models;
using Microsoft.AspNetCore.Http;
using System.IO;
using Newtonsoft.Json;
using System.Text;

namespace Opdracht4Chat.Controllers
{
    public class HomeController : Controller
    {
        List<Userss> userList;
        string name = "tss";
        [HttpGet]
        public ViewResult Index()
        {
            return View();
        }

        [HttpPost]
        public ViewResult Index(Login pLoginData)
        {
            Debug.WriteLine("teestt");
            HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(string.Format("https://localhost:5000/api/user"));

            webReq.Method = "GET";

            HttpWebResponse webResp = (HttpWebResponse)webReq.GetResponse();

            string jsonString;
            using (Stream stream = webResp.GetResponseStream())
            {
                StreamReader sr = new StreamReader(stream, System.Text.Encoding.UTF8);
                jsonString = sr.ReadToEnd();
                Debug.WriteLine(jsonString);
            }
            userList = JsonConvert.DeserializeObject<List<Userss>>(jsonString);


            Userss result = userList.Find(x => x.Username == pLoginData.Username);
            if(result != null) { 

            if (result.Password == pLoginData.Password)
                {
                    // Sessievariabele aanmaken met daarin de situatie van het inloggen.
                    HttpContext.Session.SetString("LoggedIn", "true");
                    HttpContext.Session.SetString("UserName", pLoginData.Username);
                    return View("chat");
                }
                else
                {
                    HttpContext.Session.SetString("LoggedIn", "false");
                    return View("index");
                }
            }
            else
                {
                    HttpContext.Session.SetString("LoggedIn", "false");
                    return View("index");
                }

        }
        [HttpPost]
        public ViewResult Register(Login pRegisterData)
        {
            if (pRegisterData.Password == pRegisterData.ConfirmPassword)
            {
                var dataa = "{\"Username\":\""+ pRegisterData.Username +"\",\"Password\":\""+ pRegisterData.Password +"\"}";
                var data = Encoding.ASCII.GetBytes(dataa);
                HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(string.Format("https://localhost:5000/api/user"));
                webReq.Method = "POST";
                webReq.ContentType = "application/json;charset=UTF-8";
                webReq.ContentLength = data.Length;
                Stream dataStream = webReq.GetRequestStream();
                dataStream.Write(data, 0, data.Length);
                dataStream.Close();
            }
            return View("index");
        }
        public ViewResult Logout()
        {
            HttpContext.Session.Clear();

            return View("Index");
        }

        public ViewResult Chat()
        {
            return View("chat");
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
