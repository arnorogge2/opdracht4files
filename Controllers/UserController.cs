using System.Collections.Generic;
using Opdracht4Chat.Models;
using Opdracht4Chat.Services;
using Microsoft.AspNetCore.Mvc;

namespace Opdracht4Chat.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserService _UsersService;

        public UserController(UserService UsersService)
        {
            _UsersService = UsersService;
        }

        [HttpGet]
        public ActionResult<List<Users>> Get()
        {
            return _UsersService.Get();
        }

        [HttpGet("{id:length(24)}", Name = "GetUsers")]
        public ActionResult<Users> Get(string id)
        {
            var users = _UsersService.Get(id);

            if (users == null)
            {
                return NotFound();
            }

            return users;
        }

        [HttpPost]
        public ActionResult<Users> Create(Users users)
        {
            _UsersService.Create(users);

            return CreatedAtRoute("GetUsers", new { id = users.Id.ToString() }, users);
        }

        [HttpPut("{id:length(24)}")]
        public IActionResult Update(string id, Users usersIn)
        {
            var users = _UsersService.Get(id);

            if (users == null)
            {
                return NotFound();
            }

            _UsersService.Update(id, usersIn);

            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var users = _UsersService.Get(id);

            if (users == null)
            {
                return NotFound();
            }

            _UsersService.Remove(users.Id);

            return NoContent();
        }
    }
}