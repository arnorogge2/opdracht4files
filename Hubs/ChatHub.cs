using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;
namespace SignalRChat.Hubs
{
    public static class UsersList
    {
        public static List<string> OnlineUsers = new List<string>();
        public static Dictionary<string, string> users = new Dictionary<string, string>();
    }

    public class ChatHub : Hub
    {
        List<string> userList = new List<string>();

        public override async Task OnConnectedAsync()
        {
            await Clients.Client(Context.ConnectionId).SendAsync("checkOnline");
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            if (UsersList.users.ContainsKey(Context.ConnectionId))
            {
            string value = UsersList.users[Context.ConnectionId];
            UsersList.OnlineUsers.Remove(value);
            }
            await Clients.All.SendAsync("RecieveOnlineUsers", UsersList.OnlineUsers);
        }

        public async Task SendMessage(string user, string message)
        {
            await Clients.All.SendAsync("ReceiveMessage", user, message);
        }

        public async Task NewUser(string userName)
        {
            UsersList.OnlineUsers.Add(userName);
            UsersList.users.Add(Context.ConnectionId, userName);
            await Clients.All.SendAsync("RecieveOnlineUsers", UsersList.OnlineUsers);
        }


    }
}