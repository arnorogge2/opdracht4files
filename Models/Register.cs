﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Opdracht4Chat.Models
{
    public class Register
    {
        [Required(ErrorMessage = "Geef een geldig username op.")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Geef je wachtwoord op.")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Geef je wachtwoord op.")]
        public string ConfirmPassword { get; set; }
    }
}
