using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Opdracht4Chat.Models
{
    public class Users
    {
        public ObjectId Id { get; set; }

        [BsonElement("Username")]
        public string Username { get; set; }

        [BsonElement("Password")]
        public string Password { get; set; }
    }
}