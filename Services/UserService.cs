using System.Collections.Generic;
using System.Linq;
using Opdracht4Chat.Models;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Opdracht4Chat.Services
{
    public class UserService
    {
        private readonly IMongoCollection<Users> _users;

        public UserService(IConfiguration config)
        {
            var client = new MongoClient(config.GetConnectionString("UsersDb"));
            var database = client.GetDatabase("UsersDb");
            _users = database.GetCollection<Users>("Users");
        }

        public List<Users> Get()
        {
            return _users.Find(users => true).ToList();
        }

        public Users Get(string id)
        {
            var docId = new ObjectId(id);

            return _users.Find<Users>(users => users.Id == docId).FirstOrDefault();
        }

        public Users Create(Users users)
        {
            _users.InsertOne(users);
            return users;
        }

        public void Update(string id, Users usersIn)
        {
            var docId = new ObjectId(id);

            _users.ReplaceOne(users => users.Id == docId, usersIn);
        }

        public void Remove(Users usersIn)
        {
            _users.DeleteOne(users => users.Id == usersIn.Id);
        }

        public void Remove(ObjectId id)
        {
            _users.DeleteOne(users => users.Id == id);
        }
    }
}