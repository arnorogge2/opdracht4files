"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/chatHub").build();

var userName = String(document.getElementById("userName").innerHTML);

  
connection.on("ReceiveMessage", function (user, message) {
    
    var msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    var encodedMsg = user + ": " + msg;
    var div = document.createElement("div");
    var att = document.createAttribute("class");
    att.value = "well";
    div.setAttributeNode(att);
    div.textContent = encodedMsg;
    document.getElementById("messagesList").appendChild(div);
});

connection.on("checkOnline", function( id){
    connection.invoke("NewUser", userName).catch(function (err) {
        return console.error(err.toString());
    });
});
connection.on("RecieveOnlineUsers", function (user) {
    document.getElementById("usersList").innerHTML = " ";
    var a = user;
    a.forEach(function(entry) {
        var div = document.createElement("div");
        var att = document.createAttribute("class");
        att.value = "well";
        div.setAttributeNode(att);
        div.textContent = entry;
        document.getElementById("usersList").appendChild(div);
    });
    
    /* var msg = user.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    var encodedMsg = msg;
    var div = document.createElement("div");
    var att = document.createAttribute("class");
    att.value = "well";
    div.setAttributeNode(att);
    div.textContent = encodedMsg;
    document.getElementById("usersList").appendChild(div); */
});

connection.start().catch(function (err) {
    return console.error(err.toString());
});

document.getElementById("sendButton").addEventListener("click", function (event) {
    var user = userName;
    var message = document.getElementById("messageInput").value;
    connection.invoke("SendMessage", user, message).catch(function (err) {
        return console.error(err.toString());
    });
    
    event.preventDefault();
});
