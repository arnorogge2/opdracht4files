document.getElementById("login-form-link").addEventListener("click", function (event) {
    $("#login-form").delay(100).fadeIn(100);
    $("#register-form").fadeOut(100);
    $('#register-form-link').removeClass('active');
    $(this).addClass('active');
    event.preventDefault();
});

document.getElementById("register-form-link").addEventListener("click", function (event) {
    $("#register-form").delay(100).fadeIn(100);
    $("#login-form").fadeOut(100);
	$('#login-form-link').removeClass('active');
	$(this).addClass('active');
    event.preventDefault();
});